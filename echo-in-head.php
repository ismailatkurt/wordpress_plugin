<?php

function echo_in_head(){
	global $post; 
	$postid = $post->ID;
	$value = get_post_meta( $postid, '_my_meta_value_key', true );
	echo '<meta property="og:title" content="'.esc_attr($value).'"/>';
}

add_action('wp_head', 'echo_in_head');


?>