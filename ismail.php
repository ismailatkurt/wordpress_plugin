<?php
/**
 * @package ismail
 * @version 1.0
 */
/*
Plugin Name: İsmail
Plugin URI: http://wordpress.org/plugins/ismail/
Description: This is a test plugin. Soon or late, it will be ready for use.
Author: İsmail Atkurt
Version: 1.0
Author URI: http://www.developernews.org
*/


add_action( 'admin_menu', 'register_my_custom_menu_page' );

function register_my_custom_menu_page(){
    add_menu_page( 'custom menu title', 'custom menu', 'manage_options', 'custompage', 'my_custom_menu_page', plugins_url( 'ismail/images/icon.png' ), 100 ); 
}

function my_custom_menu_page(){
	add_option('deneme','deneme değeri','','yes');
	echo '<div class="wrap">';
	$myval = get_option('deneme');
	echo $myval;
	echo '</div>';
}

include 'post-meta-boxes.php';
include 'echo-in-head.php';

?>
